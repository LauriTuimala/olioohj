package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class kinoLister {
  private HashMap<String, String> mappen;
  private Document doc;
  private ArrayList<theater> theaters = new ArrayList<theater>();
  static private kinoLister kinot= null;
  private NodeList nodes= null;
  private ArrayList<movie> movies = new ArrayList<movie>();
  private ArrayList<movie> moviesSortedByName= new ArrayList<movie>();
 public static kinoLister getLister(){
     if (kinot == null){
    	 kinot = new kinoLister();
     }
     return kinot;
}
 
 public void clearMovies(){
	 movies.clear();
 }
 public ArrayList<movie> getMovies(){
	 return movies;
 }
 public Document getDoc(){
	 return doc;
 }
 private void contentParser(URL url) throws IOException{
	 BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
	 String page="";
	 String line;
	 while ((line =br.readLine())!= null){
		 page += line + "\n";
	 }
	 System.out.print(page);
 }
 public void parseMovieListing(URL urlikka){

	  mappen= new HashMap<String, String>();
	  InputStream inputStream;
	  try {
		 //contentParser(urlikka);
		//  System.out.println(urlikka.getContent().getClass());
		  inputStream = (InputStream)urlikka.getContent();
		  DocumentBuilderFactory docbf = DocumentBuilderFactory.newInstance();
		  docbf.setNamespaceAware(true);
		  DocumentBuilder docbuilder;
		  docbuilder = docbf.newDocumentBuilder();
		  doc = docbuilder.parse(inputStream);
		  doc.getDocumentElement().normalize();
	} catch (IOException | ParserConfigurationException | SAXException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	  nodes = doc.getElementsByTagName("Show");
	  int i =1;
	 for (i=1 ; i< nodes.getLength();i++){
		Node cycle =nodes.item(i);
		Element e = (Element) cycle;
	//	System.out.println(e);
		mappen.put("Start", getMagicValue("dttmShowStart", e));
		mappen.put("End", getMagicValue("dttmShowEnd", e));
		mappen.put("Title", getMagicValue("OriginalTitle", e));
		mappen.put("Duration", getMagicValue("LengthInMinutes", e));
		movie jahveh = new movie((String)mappen.get("Start"), (String)mappen.get("End"),mappen.get("Title"),(String)mappen.get("Duration"));
		
		movies.add(jahveh);
	 }
}
 private String getMagicValue(String tag, Element e){
	 return((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
 }
 public void sortMoviesbyStart(String starttime){
		int r= 0;
		ArrayList<movie> moviesSorted = new ArrayList<movie>();
		
		for(r=0; r < movies.size();r++){
			if ((starttime.compareToIgnoreCase(movies.get(r).getStart()))<= 0){
				moviesSorted.add(movies.get(r));
			}
	}
		if(moviesSorted.isEmpty()){
			movie prkl = new movie("0T00:00","0T00:00", "Elokuvia ei löytynyt", "00");
			moviesSorted.add(prkl);
		}
		movies= moviesSorted;
 }
 public void sortMoviesbyEnd(String endtime){
		int r= 0;
		ArrayList<movie> moviesSortedByEnd = new ArrayList<movie>();
		
		for(r=0; r < movies.size();r++){
			if ((endtime.compareToIgnoreCase(movies.get(r).getEnd()))>= 0){
				moviesSortedByEnd.add(movies.get(r));
			}
	}
		if(moviesSortedByEnd.isEmpty()){
			movie prkl = new movie("0T00:00","0T00:00", "Elokuvia ei löytynyt", "00");
			moviesSortedByEnd.add(prkl);
		}
		movies= moviesSortedByEnd;
}
 public void addMovie(movie mov){
	 movies.add(mov);
 }
 
 
 public void getXMLfile(URL u){
 try {

	 InputStream inputStream = (InputStream)u.getContent();
	 DocumentBuilderFactory docbf = DocumentBuilderFactory.newInstance();
	 docbf.setNamespaceAware(true);
	 DocumentBuilder docbuilder;
	 docbuilder = docbf.newDocumentBuilder();	
	 doc = docbuilder.parse(inputStream);
	 doc.getDocumentElement().normalize();
 } catch (SAXException | IOException | ParserConfigurationException e ) {
		// TODO Auto-generated catch block
		e.printStackTrace();
 }
 }
 public String performNameSearch(String searchable){
	 //etsii kaikista teattereista elokuvaa x
	 int j=0;
	 String medusa="00";
	 URL findus;
	 for (j=0; j< theaters.size(); j++){
		try {
			findus= new URL("http://www.finnkino.fi/xml/Schedule/?area="+theaters.get(j).getId());
			parseMovieListing(findus);
			medusa=fixMovielistings(searchable, theaters.get(j).getLocation());
			clearMovies();
			} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	 movies = moviesSortedByName;
	 if(movies.isEmpty()){
		movie prkl = new movie("00T00:00","00T00:00","-", "00");
		movies.add(prkl);
		String flicker= "Hakuehdoilla ei löytyny elokuvia";
		return flicker;
	 }
	 String boheme = movies.get(0).getTitle();
	 for(j=0; j < movies.size();j++){ 
		 movies.get(j).setTitle(movies.get(j).getDuration());
		 movies.get(j).setDuration(medusa);
	 }
	 return boheme;
	
 }
 public String fixMovielistings(String seek, String loc){
		//etsii haettavaa stringiä elokuvan nimestä, lisää listaan jos löytyy
	 	int r= 0;
	 	String fluff= "00";
		for(r=0; r < movies.size();r++){
			if (movies.get(r).getTitle().contains(seek)){
				fluff = movies.get(r).getDuration();
				movies.get(r).setDuration(loc);
				moviesSortedByName.add(movies.get(r));
			}
	}
		return fluff;
		
}
 
 
 
 public ArrayList<theater> getTheaterListings(){
	 return theaters;
 }
 
 public String findTheaterId(String locate){
	 int j=0;
	 for (j=0; j< theaters.size(); j++){
		 if (locate.equals(theaters.get(j).getLocation())){
			 return theaters.get(j).getId();
		 }
	 }
	String k = ("TheaterIdNotFound");
	return k;
 }

 
public void parseTheaterListing(){
	 nodes = doc.getElementsByTagName("TheatreArea");
	int i =1;
	 for (i=1 ; i< nodes.getLength();i++){
		Node cycle =nodes.item(i);
		theater getsome = new theater((String)cycle.getTextContent().trim().subSequence(0, 4), ((String)cycle.getTextContent().trim().subSequence(5, cycle.getTextContent().trim().length())).trim());
		theaters.add(getsome);
	 }
}
 
 
 
}
