package application;

public class movie {
	private String start;
	private String end;
	private String title;
	private String duration;


	public movie(String s, String n, String m, String p){
		String[] parts = s.split("T");
		start = parts[1].substring(0,5); 
		String[] spares = n.split("T");
	    end = spares[1].substring(0, 5); 
		title= m;
		duration= p;
	}
	public movie(String titteli){
		start ="o";
		end="o";
		title = titteli;
		duration= "zero";
	}
	public String getStart(){
		return start;
	}
	public String getTitle(){
		return title;
	}
	public String getDuration(){
		return duration;
	}
	public void setDuration(String rep){
		duration = rep;
	}
	public void setTitle(String replacement){
		title = replacement;
	}
	public String getEnd(){
		return end;
	}
	@Override
	public String toString(){
		String f = start+ "-"+end+"  : "+title+ " ("+duration+" minutes)";	
		return f;
	}
}
