package application;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class finnKinoController implements Initializable{

	
@FXML
private ComboBox<theater> theaterPicker;

@FXML
private Label theaterPrompt;

@FXML
private Label intervalMarker;

@FXML
private Label queryPrompt;

@FXML
private TextField intervalBegin;

@FXML
private TextField intervalEnd;

@FXML
private TextField programDate;	

@FXML
private TextField queryInputField;

@FXML
private Button listMovies;
	
@FXML
private Button nameSearch;

//@FXML
//private TextArea console;

@FXML
private ListView<movie> movieList;

@FXML
private void performListingAction(ActionEvent event){
	//elokuvien listaus
	kinoLister finski= kinoLister.getLister();
	finski.clearMovies();
	theaterPicker.getValue().getId();
	if ( theaterPicker.getValue() == null ){
		movie Error= new movie("-", "-","Ole hyvä ja tarkista parametrit", "-");
		finski.getMovies().add(Error);
		movieList.setItems(FXCollections.observableArrayList(finski.getMovies()));
	}else if(programDate.getText().isEmpty()|| programDate.getText().equals(programDate.getPromptText())){
		//tämä päivä
		try {
			URL jaa = new URL("http://www.finnkino.fi/xml/Schedule/?area="+theaterPicker.getValue().getId());
			finski.parseMovieListing(jaa);
			Document docker= finski.getDoc();
			movieList.setItems(FXCollections.observableArrayList(finski.getMovies()));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}else{
		//joku päivä
		URL juu;
		try {
			juu = new URL("http://www.finnkino.fi/xml/Schedule/?area="+theaterPicker.getValue().getId()+"&dt="+programDate.getText());
	    //	console.appendText(theaterPicker.getValue().getId() + " "+ programDate.getText());
			finski.getXMLfile(juu);
			finski.parseMovieListing(juu);
			Document docker= finski.getDoc();
			//console.appendText(docker.getFirstChild().getFirstChild().getAttributes().toString());
			movieList.setItems(FXCollections.observableArrayList(finski.getMovies()));
			//console.appendText("moi");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	if(((intervalBegin.getText().isEmpty())!= true)&& (intervalBegin.getText().equals(intervalBegin.getPromptText())!= true)) {
		finski.sortMoviesbyStart(intervalBegin.getText());
		movieList.setItems(FXCollections.observableArrayList(finski.getMovies()));
	}
	if(((intervalEnd.getText().isEmpty())!= true)&& (intervalEnd.getText().equals(intervalEnd.getPromptText())!= true)) {
		finski.sortMoviesbyEnd(intervalEnd.getText());
		movieList.setItems(FXCollections.observableArrayList(finski.getMovies()));
	}
}


@FXML
private void performNameQuery(ActionEvent event){
	kinoLister finski= kinoLister.getLister();
	if(((queryInputField.getText().isEmpty())!= true)&& (queryInputField.getText().equals(queryInputField.getPromptText())!= true)){
		String headline=  finski.performNameSearch(queryInputField.getText());
		ArrayList<movie> itemset= new ArrayList<movie>();	
		 movie juups = new movie(headline);
		itemset.add(juups);
		 int i= 0;
		 for(i= 0; i<finski.getMovies().size();i++){
			 itemset.add(finski.getMovies().get(i));
		 }
		movieList.setItems(FXCollections.observableArrayList(itemset));
	}else{
		finski.clearMovies();
		movie rorrim = new movie("0T00:00","0T00:00"," Anna haettavan elokuvan nimi!", "00");
		finski.addMovie(rorrim);
		movieList.setItems(FXCollections.observableArrayList(finski.getMovies()));
	}
}
//public void troubleshooter(String s){
	//console.appendText(s);
//}
@FXML
private void populateComboBox(){
	kinoLister finski= kinoLister.getLister();
	URL url;
	try {
		url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
		finski.getXMLfile(url);
		finski.parseTheaterListing();
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	theaterPicker.setItems(FXCollections.observableArrayList(finski.getTheaterListings()));
//	console.setText(finski.getTheaterListings().get(3).getId() +" tämä tähän "+finski.getTheaterListings().get(3).getLocation());
	}


@Override
public void initialize(URL url, ResourceBundle rb) {

	populateComboBox();
		
	
	
}
}
