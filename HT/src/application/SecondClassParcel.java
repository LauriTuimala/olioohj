package application;

public class SecondClassParcel extends Parcel {
	
	public SecondClassParcel(Item k){
		super();
		contents = k;
		quality = 2;
	}

}
