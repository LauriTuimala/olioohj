package application;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/*Tämä classi hoitaa tietokantayhteydet ja hallinnoi paketteja sekä esineitä*/
public class SmartRunner {

private HashMap<String, String> mappen;
private NodeList nodes= null;
private Document doc;
static private SmartRunner setup= null;
//ajon aikaiset listat
private ArrayList<Item> esineet = new ArrayList<Item>();
private ArrayList<Parcel> paketit= new ArrayList<Parcel>();
//Tietokannan osoite tässä: Muista tarkistaa osoite ennen ojelman ajoa.
private String dbAddress ="jdbc:sqlite:/net/homes/u041/tuimala/Olio-ohjelmointi graffat/HT/MailDB.sqlite";
ActionLog logger = ActionLog.getLister(); 

//Singleton
public static SmartRunner getLister(){
    if (setup == null){
   	 setup = new SmartRunner();
    }
    return setup;
}
//Esimerkkiesineet, arraylistiin ja tietokantaan
public void setItems(){
	Item one =new Item("Pöytä", "Suuri", "Ei");
	esineet.add(one);
	Item two =new Item("Maljakko", "Pieni", "Kyllä");
	esineet.add(two);
	Item three =new Item("PC", "Medium", "Kyllä");
	Item four =new Item("Taulutelevisio", "Suuri", "Kyllä");
	Item five =new Item("Alasin", "medium", "Ei");
	esineet.add(three);
	esineet.add(four);
	esineet.add(five);
	putItems();
}

//GUI:ta varten, esineiden nimet erilliseen listaan
public ArrayList<String> getItemNames(){
	ArrayList<String> names= new ArrayList<String>(); 
	int i;
	String flipflop;
	for(i=0;i<esineet.size();i++){
		flipflop= esineet.get(i).getName();
		names.add(flipflop);
	} 
	return names;
}

//esinelistan get
public ArrayList<Item>getItems(){
	return esineet;
}
//esineet-listan tyhjennys
public void clearItemList(){
	esineet.clear();
}
//paketit-listan tyhjennys
public void clearParcelList(){
	paketit.clear();
}

//metodi esineen etsimiseen ID: perusteella
public Item findItemByID(int findableID){
	int i;
	for(i=0;i<esineet.size();i++){
		if (findableID == esineet.get(i).getItemID()){
			return esineet.get(i);
		}
	}
	return esineet.get(i);
}

//metodi esineen etsimiseen nimen perusteella, gui:ta varten
public Item findItem(String findablename){
	int i;
	for(i=0;i<esineet.size();i++){
		if (findablename.equals(esineet.get(i).getName())){
			return esineet.get(i);
		}
	}
	return esineet.get(i);
}
//metodi paketin etsimiseen ID:n perusteella
public Parcel findParcelByID(int findableID){
	int i;
	for(i=0;i<paketit.size();i++){
		if (findableID == paketit.get(i).getParcelID()){
			return paketit.get(i);
		}
	}
	return paketit.get(i);
}


//heittää esineet-listan sisällön tietokantaan
public void putItems(){
	int i;
	for(i=0;i<esineet.size();i++){
		addItemtoDb(esineet.get(i));
		
	}

}
//pakettilistan get
public ArrayList<Parcel> getParcels(){
	return paketit;
}
//välimetodi ohjelman alussa esineiden ajamiseksi tietokantaan, jos tarpeellista
public void StartWithItems(){
	ArrayList<Item> throwAway = getItemsFromDB();
}

//päivittää paketit-listan tietyn paketin tiedot lähetettäessä
public void UpdateParcelInfo(Parcel extant){
	Parcel updating = findParcelByID(extant.getParcelID());
	updating.setLocation(extant.getParcelLocation());
	updating.setDestination(extant.getDestination());
	updating.setPortOfCall(extant.getPortOfCall());
}

//metodi hakee netistä smartpost-datan, heittää sen tietokantaan (hashmap välissä harjoituksen vuoksi)
public void parseOfficeListing() throws MalformedURLException{
		  URL urlikka = new URL("http://smartpost.ee/fi_apt.xml");
		  mappen= new HashMap<String, String>();
		  InputStream inputStream;
		  try {

			  inputStream = (InputStream)urlikka.getContent();
			  DocumentBuilderFactory docbf = DocumentBuilderFactory.newInstance();
			  docbf.setNamespaceAware(true);
			  DocumentBuilder docbuilder;
			  docbuilder = docbf.newDocumentBuilder();
			  doc = docbuilder.parse(inputStream);
			  doc.getDocumentElement().normalize();
		  } catch (IOException | ParserConfigurationException | SAXException e1) {
		     logger.Logging("Error : "+e1.getClass().getName() + ":ParceOfficeListing " + e1.getMessage());
			 logger.PrintLog();
			e1.printStackTrace();
		  }
		  nodes = doc.getElementsByTagName("place");
		  int i =1;
		  for (i=1 ; i< nodes.getLength();i++){
			Node cycle =nodes.item(i);
			Element e = (Element) cycle;
			//hashmapping for fun & profit
			mappen.put("Code", getMagicValue("code", e));
			mappen.put("City", getMagicValue("city", e));
			mappen.put("Address", getMagicValue("address", e));
			mappen.put("OpenHours", getMagicValue("availability", e));
			mappen.put("Poffice", getMagicValue("postoffice", e));
			mappen.put("Lat", getMagicValue("lat", e));
			mappen.put("Lng", getMagicValue("lng", e));
			Office jahveh = new Office((String)mappen.get("Code"), (String)mappen.get("City"),(String)mappen.get("Address"),(String)mappen.get("OpenHours"),(String)mappen.get("Poffice"),(String)mappen.get("Lat"),(String)mappen.get("Lng"));
			AddOfficeToDb(jahveh);
		 }
		  logger.Logging("Added Smartposts from the 'Net to Db");
}

//lisää esineen tietokantaan
public void addItemtoDb (Item addable){
	Connection c = null;
    Statement stmt = null;
    
    try {
       Class.forName("org.sqlite.JDBC");
       c = DriverManager.getConnection(dbAddress);

       stmt = c.createStatement();
       String sql =  "INSERT INTO ITEMS (ITEMID,NAME,SIZE,DURABLE,BROKEN) " +
             "VALUES ("+addable.getItemID()+","+"'"+addable.getName()+"'"+","+"'"+addable.getSize()+"'"+","+"'"+addable.getFragile()+"'"+","+"'"+addable.getBroken()+"'"+");"; 
       stmt.executeUpdate(sql);
       stmt.close();
       c.close();
       logger.Logging("Added Item " +addable.toString()+ " to Db");
	} catch ( Exception e ) {
    System.err.println( e.getClass().getName() + ": addItemtoDb" + e.getMessage() );
    logger.Logging("Error : "+e.getClass().getName() + ":addItemtoDb " + e.getMessage());
	logger.PrintLog();
    System.exit(0);
	}
       
       
}

//lisätään paketti tietokantaan
public void addParceltoDb (Parcel addable){
	Connection c = null;
    Statement stmt = null;
    
    try {
       Class.forName("org.sqlite.JDBC");
       c = DriverManager.getConnection(dbAddress);

       stmt = c.createStatement();
       String sql =  "INSERT INTO PACKAGE (PACKAGEID,LOCATION,QUALITY,SIZE,CONTENTS,DESTINATION,PORTOFCALL) " +
             "VALUES ("+addable.getParcelID()+","+addable.getParcelLocation()+","+addable.getQuality()+","+"'"+addable.getSizeLimit()+"'"+","+"'"+addable.getParcelItemID()+"'"+","+"'"+addable.getDestination()+"'"+","+"'"+addable.getPortOfCall()+"'"+");"; 
       stmt.executeUpdate(sql);
       stmt.close();
       c.close();
       logger.Logging("Added Parcel " +addable.toString()+ " to Db");
    } catch ( Exception e ) {
       System.err.println( e.getClass().getName() + ": addParceltoDb" + e.getMessage() );
       logger.Logging("Error : "+e.getClass().getName() + ":addParceltoDb " + e.getMessage());
	   logger.PrintLog();
       System.exit(0);
    }
}

//lisätään smartpost-toimipaikka tietokantaan, tätä käyttää vain parseOfficeListing()
private void AddOfficeToDb(Office entrant){
	Connection c = null;
    Statement stmt = null;
    
    try {
       Class.forName("org.sqlite.JDBC");
       c = DriverManager.getConnection(dbAddress);

       stmt = c.createStatement();
       String sql =  "INSERT INTO OFFICES (ID,CODE,CITY,ADDRESS,OPENHOURS,POFFICE,LAT,LNG)" +
               "VALUES ("+entrant.getID()+","+"'"+entrant.getCode()+"'"+","+"'"+entrant.getCity()+"'"+","+"'"+entrant.getAddress()+"'"+","+"'"+entrant.getOpenHours()+"'"+","+"'"+entrant.getPoffice()+"'"+","+"'"+entrant.getLat()+"'"+","+"'"+entrant.getLng()+"'"+");"; 
       stmt.executeUpdate(sql);
       stmt.close();
       c.close();
    } catch ( Exception e ) {
       System.err.println( e.getClass().getName() + ":AddOfficeToDb " + e.getMessage() );
       logger.Logging("Error : "+e.getClass().getName() + ":AddOfficeToDb " + e.getMessage());
	   logger.PrintLog();
       System.exit(0);
    }
}
//päivittää paketin sijainnin ja lähtö- sekä tulopisteet tietokantaan
public void UpdateParcelCoordsDb(Parcel addable){
	Connection c = null;
    Statement stmt = null;
    	try {
	       Class.forName("org.sqlite.JDBC");
	       c = DriverManager.getConnection(dbAddress);

	       stmt = c.createStatement();
	       String sql =  "UPDATE PACKAGE set LOCATION ="+addable.getParcelLocation()+", DESTINATION ="+addable.getDestination()+", PORTOFCALL="+addable.getPortOfCall()+" where PACKAGEID ="+addable.getParcelID()+ ");"; 
	       stmt.executeUpdate(sql);
	       stmt.close();
	       c.close();
	    } catch ( Exception e ) {
	       System.err.println( e.getClass().getName() + ":UpdateParcelCoordsDb " + e.getMessage() );
	       logger.Logging("Error : "+e.getClass().getName() + ":UpdateParcelCoordsDb " + e.getMessage());
		   logger.PrintLog();
	       System.exit(0);
	    }
	
	
	
}
//poistaa halutun esineen tietokannasta
public void DeleteItemFromDb(Item deletable){
	Connection c = null;
    Statement stmt = null;
    	try {
	       Class.forName("org.sqlite.JDBC");
	       c = DriverManager.getConnection(dbAddress);

	       stmt = c.createStatement();
	       String sql =  "DELETE FROM ITEMS  where ITEMID ="+deletable.getItemID()+ ");"; 
	       stmt.executeUpdate(sql);
	       stmt.close();
	       c.close();
	       logger.Logging("Deleted Item " +deletable.getName()+ " from Db");
	    } catch ( Exception e ) {
	       System.err.println(e.getClass().getName() + ":DeleteItemFromDb " + e.getMessage());      
	       logger.Logging("Error : "+e.getClass().getName() + ":DeleteItemFromDb " + e.getMessage());
		   logger.PrintLog();
	       System.exit(0);
	    }
	
	
	
}


//poistaa halutun paketin tietokannasta
public void DeleteParcelFromDb(Parcel deletable){
	Connection c = null;
    Statement stmt = null;
    	try {
	       Class.forName("org.sqlite.JDBC");
	       c = DriverManager.getConnection(dbAddress);

	       stmt = c.createStatement();
	       String sql =  "DELETE FROM PACKAGE  where PACKAGEID ="+deletable.getParcelID()+ ");"; 
	       stmt.executeUpdate(sql);
	       stmt.close();
	       c.close();
	       logger.Logging("Deleted Parcel " +deletable.toString()+ " from Db");
	    } catch ( Exception e ) {
	       System.err.println( e.getClass().getName() + ":DeleteParcelFromDb " + e.getMessage() );
	       logger.Logging("Error : "+e.getClass().getName() + ":DeleteParcelFromDb " + e.getMessage());
		   logger.PrintLog();
	       System.exit(0);
	    }
	
	
	
}
//poistaa halutun toimisto tietokannasta
public void DeleteOfficeFromDb(Office deletable){
	Connection c = null;
  Statement stmt = null;
  	try {
	       Class.forName("org.sqlite.JDBC");
	       c = DriverManager.getConnection(dbAddress);

	       stmt = c.createStatement();
	       String sql =  "DELETE FROM OFFICES  where ID ="+deletable.getID()+ ");"; 
	       stmt.executeUpdate(sql);
	       stmt.close();
	       c.close();
	       logger.Logging("Deleted Office " +deletable.toString()+ " from Db");
	    } catch ( Exception e ) {
	       System.err.println( e.getClass().getName() + ":DeleteOfficeFromDb " + e.getMessage() );
	       logger.Logging("Error : "+e.getClass().getName() + ":DeleteOfficeFromDb " + e.getMessage());
		   logger.PrintLog();
	       System.exit(0);
	    }
	
	
	
}
//hakee tietokannasta kaikki paketit
public ArrayList<Parcel> getParcelsFromDB(){
	   Connection c = null;
	   Statement stmt = null;
	  
	   ArrayList<Parcel> ParcelList = new ArrayList<>();
	   try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection(dbAddress);
	      c.setAutoCommit(false);

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery("SELECT * FROM PACKAGE;");
	      
	      while ( rs.next() ) {
	    	  Parcel adder= new Parcel();
	    	  adder.ParcelID = rs.getInt("PACKAGEID");
	          adder.location = rs.getInt("LOCATION");
	          adder.quality = rs.getInt("QUALITY");
	          adder.sizelimit = rs.getString("SIZE");
	          adder.contents= findItemByID(rs.getInt("CONTENTS"));	
	          adder.destination = rs.getString("DESTINATION");
	          adder.portofcall = rs.getString("PORTOFCALL");
	          ParcelList.add(adder);
	          if (!paketit.contains(adder)){
	        	  paketit.add(adder);
	          }
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	      logger.Logging("Fetched "+ParcelList.size()+" Parcels from Db");
	   } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ":getParcelsFromDb " + e.getMessage() );
	      logger.Logging("Error : "+e.getClass().getName() + ":getParcelsFromDb " + e.getMessage());
	      logger.PrintLog();
	      System.exit(0);
	   }
	   //suoritetaan ID-arvon uudelleenmääritys suuremmaksi kuin olemassaolevat paketit ettei tule josssain vaiheessa duplikaatteja tietokantaan
	   for (Parcel foo: ParcelList){
		   if( foo.getParcelID()>= foo.getParcelCounter()){
			   foo.setParcelCounter(foo.getParcelID());
		   } 
	   }
	   return ParcelList;
}

//hakee tietokannasta kaikki esineet
public ArrayList<Item> getItemsFromDB(){
	   Connection c = null;
	   Statement stmt = null;
	  
	   ArrayList<Item> ItemList = new ArrayList<>();
	   try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection(dbAddress);
	      c.setAutoCommit(false);

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery("SELECT * FROM ITEMS;");
	      
	      while ( rs.next() ) {
	    	  Item adder= new Item();
	    	  adder.ItemID = rs.getInt("ITEMID");
	          adder.name = rs.getString("NAME");
	          adder.size = rs.getString("SIZE");
	          adder.Fragile = rs.getString("DURABLE");
	          adder.Broken = rs.getBoolean("BROKEN");
	          ItemList.add(adder);
	          if (!esineet.contains(adder)){
	        	  esineet.add(adder);
	          }
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	      logger.Logging("Fetched "+ItemList.size()+" items from Db");
	   } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ":getItemsFromDb " + e.getMessage() );
	      logger.Logging("Error : "+e.getClass().getName() + ":getItemsFromDb " + e.getMessage());
	      logger.PrintLog();
	      System.exit(0);
	   }
	   //suoritetaan ID-arvon uudelleenmääritys suuremmaksi kuin olemassaolevat paketit ettei tule josssain vaiheessa duplikaatteja tietokantaan
	   for (Item foo: ItemList){
		   if( foo.getItemID()>= foo.getItemCounter()){
			   foo.setItemCounter(foo.getItemID());
		   } 
	   }
	   return ItemList;
}

//hakee tietokannasta kaikki tietyn kaupungin smartpostit, lisää ne arraylistiin ja palauttaa GUI:lle
public ArrayList<Office> getSelectCityFromDb(String selectedCity){
	   Connection c = null;
	   Statement stmt = null;
	  
	   ArrayList<Office> CityNodes = new ArrayList<>();
	   try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection(dbAddress);
	      c.setAutoCommit(false);

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery("SELECT * FROM OFFICES WHERE CITY= " +"'"+selectedCity+"';");
	      
	      while ( rs.next() ) {
	    	  Office adder = new Office();
	    	  adder.ID = rs.getInt("ID");
	          adder.Code = rs.getString("code");
	          adder.City = rs.getString("city");
	          adder.Address = rs.getString("address");
	          adder.OpenHours = rs.getString("openhours");
	          adder.Poffice = rs.getString("poffice");
	          adder.Lat = rs.getString("lat");
	          adder.Lng = rs.getString("lng");
	          CityNodes.add(adder);
	      }
	      rs.close();
	      stmt.close();
	      c.close();    
	   } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ":getSelectCityFromDb " + e.getMessage() );
	      logger.Logging("Error : "+e.getClass().getName() + ":getSelectCityFromDb " + e.getMessage());
	      logger.PrintLog();
	      System.exit(0);
	   }
	   return CityNodes;
 }
//Hakee kaupunkien nimet (yhden per) tietokannasta, pistää ne arraylistiin
public ArrayList<String> getCityNames(){
	   Connection c = null;
	   Statement stmt = null;

	   ArrayList<String> Cities = new ArrayList<>();
	   try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection(dbAddress);
	      c.setAutoCommit(false);

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery( "SELECT DISTINCT CITY FROM OFFICES ORDER BY CITY;" );
	      
	      while ( rs.next() ) {
	    	  Cities.add(rs.getString("City"));
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	   } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ":getCityNames " + e.getMessage() );
	      logger.Logging("Error : "+e.getClass().getName() + ":getCityNames " + e.getMessage());
	      logger.PrintLog();
	      System.exit(0);
	   }
	   return Cities;
}
//poistaa sisällön taulusta, jonka nimi annetaan metodille
public void DeleteTableContents(String s){
	   Connection c = null;
	   Statement stmt = null;

	   try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection(dbAddress);
	      c.setAutoCommit(true);
	      stmt = c.createStatement();
	      stmt.executeUpdate("DELETE FROM" +"'"+s+"'"+" ;");
	      stmt.close();
	      c.close();
	      logger.Logging("Deleted table " +s+ " from Db");
	   } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ":DeleteTableContents " + e.getMessage() );
	      logger.Logging("Error : "+e.getClass().getName() + ":DeleteTableContents " + e.getMessage());
	      logger.PrintLog();
	      System.exit(0);
	   }
}
//hashmapin parseri
private String getMagicValue(String tag, Element e){
	 return((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
}
}
