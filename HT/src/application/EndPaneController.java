package application;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class EndPaneController implements Initializable{
	SmartRunner setup = SmartRunner.getLister();
	ActionLog logger = ActionLog.getLister();
@FXML
private Button EndAffirmative;
@FXML
private Button EndDenial;
@FXML
private Label endQuestion;

@FXML
//tallennetaan esineet ja paketit jso käyttäjä niin haluaa
private void AffirmativeAction(ActionEvent action){
	setup.DeleteTableContents("ITEMS");
	//muokataan ID:t järkeviksi ennen tietokantaan tunkemista
	int j= 1;
	for(Item append : setup.getItems()){
		append.setItemID(j);
		setup.addItemtoDb(append);
		j++;
	}
	setup.DeleteTableContents("PACKAGE");
	//muokataan ID:t järkeviksi ennen tietokantaan tunkemista
	int i = 1;
	for(Parcel appendix : setup.getParcels()){
		appendix.setParcelID(i);
		setup.addParceltoDb(appendix);
		i++;
	}
	logger.PrintLog();
	closeThisStage(action);
	
}
@FXML
//muuten vain suljetaan ikkuna
private void DenialAction(ActionEvent action){
	logger.PrintLog();
	closeThisStage(action);
}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		
	}
	public void closeThisStage(ActionEvent action){
		 Node  source = (Node)  action.getSource(); 
		 Stage stage  = (Stage) source.getScene().getWindow();
	    stage.close();
	}
	
	
}
