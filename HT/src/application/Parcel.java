package application;



public class Parcel {
	
boolean BreakStuff= false;
String sizelimit;
int ParcelID;
int location;
Item contents;
String destination;
String portofcall;
static int  ParcelCounter = 0;
int quality;
public Parcel(){
	ParcelID = ParcelCounter;
	ParcelCounter++;
}

public int getParcelID(){
	return ParcelID;
}
public int getParcelLocation(){
	return location;
}
public String getParcelItemName(){
	return contents.getName();
}
public int getParcelItemID(){
	return contents.getItemID();
}
public Item getContents(){
	return contents;
}
public String getDestination(){
	return destination;
}
public int getQuality(){
	return quality;
}
public boolean getBreakStuff(){
return BreakStuff;
}
public String getSizeLimit(){
return contents.getSize();
}
public String getPortOfCall(){
	 return portofcall;
}
public void setParcelID(int i){
	ParcelID = i;
}
public void setPortOfCall(String s){
	portofcall = s;
}
public void setDestination(String s){
	destination = s;
}
public void setLocation(int j){
	location = j;
}
public int getParcelCounter(){
	return ParcelCounter;
}
public void setParcelCounter(int limit){
	ParcelCounter = limit + 1;
}
@Override
public String toString(){
	String foo = ParcelID+" "+contents.getName()+" "+contents.getSize();
	return foo;
}
}
