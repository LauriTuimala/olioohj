package application;

public class Office {
int ID;
String Code;
String City;
String Address;
String OpenHours;
String Poffice;
String Lat ;
String Lng;
static int  OfficeCounter = 0;

public Office(){
	ID = OfficeCounter;
	OfficeCounter++;;
}

public Office(String c, String ci, String a, String o, String p, String lat, String lng){
	ID = OfficeCounter;
	OfficeCounter++;;
	Code= c;
    City= ci;
    Address=a;
	OpenHours= o;
	Poffice= p;
	Lat = lat;
    Lng= lng;
	}
public int getID(){
	return ID;
}
public String getCode(){
	return Code;
}

public String getCity(){
	return City;
}

public String getAddress(){
	return Address;
}

public String getOpenHours(){
	return OpenHours;
}

public String getPoffice(){
	return Poffice;
}

public String getLat(){
	return Lat;
}

public String getLng(){
	return Lng;
}
@Override
public String toString(){
	return Address;
}

}
