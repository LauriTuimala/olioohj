package application;

public class Item {
 String name;
 String size;
 String Fragile;
 int ItemID;
 boolean Broken = false;
 static private int ItemCounter = 0;
 public Item(){
	 ItemID=ItemCounter;
	 ItemCounter++;
 }
 
 public Item(String s, String r, String j){
	 ItemID = ItemCounter;
	 name = s;
	 size = r;
	 Fragile= j;

	 ItemCounter ++;
 }
 public String getName(){
	 return name;
 }
 public String getSize(){
	 return size;
 }
 public int getItemID(){
	 return ItemID;
 }
 public String getFragile(){
	 return Fragile;
 }
 public boolean getBroken(){
	 return Broken;
 }
 public void setBroken(){
	 Broken = true;
 }
public int getItemCounter(){
	return ItemCounter;
}
public void setItemID(int foo){
	ItemID = foo;
}
public void setItemCounter(int limit){
	ItemCounter = limit +1;
}
@Override
public String toString(){
	String foo = this.getName()+" "+this.getSize();
	return foo;
}
}
