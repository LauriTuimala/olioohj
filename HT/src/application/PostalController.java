package application;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
/*TODO -Pakettien sijainnin päivitys*/

public class PostalController implements Initializable{
	ArrayList<String> checker = new ArrayList<>();
	SmartRunner setup = SmartRunner.getLister();
	ArrayList<Office> UseableOffices = new ArrayList<>(); 
	ArrayList<Parcel> ParcelList = new ArrayList<>();
	ArrayList<String> UseableCities = new ArrayList<>();
	ActionLog logger = ActionLog.getLister();
@FXML
private WebView mapSlinger;
@FXML
private ComboBox<String> CitySelector;
@FXML
private ComboBox<Parcel> ParcelSelector;
@FXML
private Button CreateParcel;
@FXML
private Button SendParcel;
@FXML
private Button AddLocations;
@FXML
private Button RemoveRoutes;
@FXML
private Button ExitButton;
@FXML
private Button UpdateParcels;
@FXML
private Button OpenDBPane;
@FXML
private Label Parcelguide;
@FXML
private ComboBox<String> SendingCityPicker;
@FXML
private ComboBox<String> ArrivalCityPicker;
@FXML
private ComboBox<Office> SendingSmartPost;
@FXML
private ComboBox<Office> ArrivalSmartPost;



@FXML// avaa uuden ikkunan jossa voi tehdä esineitä, paketteja
private void CreateParcelAction(ActionEvent action){
	try {
		Stage ParcelCreation = new Stage();
		Parent page= FXMLLoader.load(getClass().getResource("ParcelCreationPane.fxml"));
		Scene scene= new Scene(page);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		ParcelCreation.setScene(scene);
		ParcelCreation.show();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
@FXML //päivittää pakettilistan
private void UpdateParcelAction(ActionEvent action){
	ParcelSelector.setItems(FXCollections.observableArrayList(setup.getParcels()));
}

@FXML//katsotaan meneekö esineet rikki (3. ja 1. luokan paketit)
private void BreakCheck(Parcel checkable){
	if (checkable.getQuality() == 1){
		if (checkable.getContents().getFragile() == "Kyllä"){
			checkable.getContents().setBroken();
			logger.Logging(checkable.getParcelItemName() +" was broken");
		} 
}else if (checkable.getQuality() == 3){
	if (checkable.getContents().getFragile() == "Kyllä"){
		checkable.getContents().setBroken();
		logger.Logging(checkable.getParcelItemName() +" was broken");
		}
	}
	
}

@FXML //Paketin lähetys
private void SendParcelAction(ActionEvent action){
	Parcelguide.setText("");
	//tarkistellaan syötteitä
	if (ParcelSelector.getValue() == null){
		Parcelguide.setText("Ole hyvä ja valitse paketti lähetettäväksi");
	}else if(SendingSmartPost.getValue()== null){
		Parcelguide.setText("Ole hyvä ja valitse lähetyspaikka");
	}else if(ArrivalSmartPost.getValue()== null){
		Parcelguide.setText("Ole hyvä ja valitse saapumispaikka");
	}else{
		//syötteet löytyy
		ArrayList<Float> coords = new ArrayList<>();
		coords.add(Float.parseFloat(SendingSmartPost.getValue().getLat()));
		coords.add(Float.parseFloat(SendingSmartPost.getValue().getLng()));
		coords.add(Float.parseFloat(ArrivalSmartPost.getValue().getLat()));
		coords.add(Float.parseFloat(ArrivalSmartPost.getValue().getLng()));
		if(ParcelSelector.getValue().getQuality()== 1){
			//ykkösluokan paketin matkarajoituscheckki
			double k=(double) mapSlinger.getEngine().executeScript("document.createPath("+coords+",'red' , "+ParcelSelector.getValue().getQuality()+")");
			if (k> 150){
				mapSlinger.getEngine().executeScript("document.deleteLastPath()");
				logger.Logging("Tried to send a first-class parcel further than 150 km");
				Parcelguide.setText("Yritit lähettää 1.luokan paketin liian kauas (>150 km)");
			}else{
				logger.Logging("Sent parcel "+ParcelSelector.getValue().getParcelID()+" From " +SendingSmartPost.getValue().getCode()+" "+SendingSmartPost.getValue().getCity()+ " To "
						+ArrivalSmartPost.getValue().getCode()+" "+ArrivalSmartPost.getValue().getCity());
				 ParcelSelector.getValue().setDestination(ArrivalSmartPost.getValue().getCode());
				 ParcelSelector.getValue().setPortOfCall(SendingSmartPost.getValue().getCode()); 
				 setup.UpdateParcelInfo(ParcelSelector.getValue());
				BreakCheck(ParcelSelector.getValue());
			}
		} else{
		//kakkos- ja kolmosluokka menee vaan	
		mapSlinger.getEngine().executeScript("document.createPath("+coords+",'red' , "+ParcelSelector.getValue().getQuality()+")");
		logger.Logging("Sent parcel "+ParcelSelector.getValue().getParcelID()+" From " +SendingSmartPost.getValue().getCode()+" "+SendingSmartPost.getValue().getCity()+ " To "
				+ArrivalSmartPost.getValue().getCode()+" "+ArrivalSmartPost.getValue().getCity());
		ParcelSelector.getValue().setDestination(ArrivalSmartPost.getValue().getCode());
		ParcelSelector.getValue().setPortOfCall(SendingSmartPost.getValue().getCode()); 
		setup.UpdateParcelInfo(ParcelSelector.getValue());
		BreakCheck(ParcelSelector.getValue());
		
		}
	}
}
	
@FXML //lisää valitun kaupungin postitoimipaikat kartalle, kaupungin valittavaksi vaihtoehdoksi paketin lähetykselle.
private void AddLocationsAction(ActionEvent action){
	ArrayList<Office> CityNodes= new ArrayList<>();
	int j;
	if ((CitySelector.getValue())!= null){
		CityNodes= setup.getSelectCityFromDb(CitySelector.getValue());
		//varmistus ettei lisätä jo olemassaolevia Smartposteja
		for(j= 0; j< UseableCities.size(); j++){
			if(CitySelector.getValue().equals(UseableCities.get(j))){
				return;
			}
		}
		//tyhjätään konttorivalitsimet koska kaupunkien indeksointi muuttui
		UseableCities.add(CitySelector.getValue());
		SendingSmartPost.getSelectionModel().clearSelection();
		ArrivalSmartPost.getSelectionModel().clearSelection();
		SendingCityPicker.setItems(FXCollections.observableArrayList(UseableCities));
		ArrivalCityPicker.setItems(FXCollections.observableArrayList(UseableCities));
		int i;
		for(i=0;i<CityNodes.size();i++){
			String first ="'"+ CityNodes.get(i).getAddress()+" , "+CityNodes.get(i).getCode()+" "+CityNodes.get(i).getCity()+"'";
			String second ="'"+ CityNodes.get(i).getPoffice()+" "+CityNodes.get(i).getOpenHours()+"'";
			System.out.println("document.goToLocation("+first+" , " +second+" , 'red')");
			mapSlinger.getEngine().executeScript("document.goToLocation("+first+" , " +second+" , 'red' )");
			UseableOffices.add(CityNodes.get(i));
		}
		logger.Logging("Added Smartposts of " +CitySelector.getValue()+" from Db");
	}


}
@FXML //Aseta lähetyskaupunki
private void SetSendingCity(ActionEvent action){
	if (SendingCityPicker.getValue() != null){
		ArrayList<Office> placeholder = new ArrayList<>();
		int j;
			for(j = 0; j<UseableOffices.size();j++){
				if(SendingCityPicker.getValue().equals(UseableOffices.get(j).getCity())){ 
					placeholder.add(UseableOffices.get(j));
			}
		}
	SendingSmartPost.setItems(FXCollections.observableArrayList(placeholder));
	}
}


@FXML //aseta saapumiskaupunki
private void SetArrivalCity(ActionEvent action){
	if (ArrivalCityPicker.getValue() != null){
		ArrayList<Office> placeholder = new ArrayList<>();
		int j;
		for(j = 0; j<UseableOffices.size();j++){
			if(ArrivalCityPicker.getValue().equals(UseableOffices.get(j).getCity())){ 
				placeholder.add(UseableOffices.get(j));
			}
		}
	ArrivalSmartPost.setItems(FXCollections.observableArrayList(placeholder));

	}
}


@FXML //poistaa kaikki reitit
private void RemoveRoutesAction(ActionEvent action){
	mapSlinger.getEngine().executeScript("document.deletePaths()");

}
@FXML //Printtaa login, lopettaa ohjelman
private void PrintLogAndExitAction(ActionEvent action){
	try {
		Stage ParcelCreation = new Stage();
		Parent page= FXMLLoader.load(getClass().getResource("EndPane.fxml"));
		Scene scene= new Scene(page);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		ParcelCreation.setScene(scene);
		ParcelCreation.show();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	closeThisStage(action);
}
//metodi ikkunan sulkemiselle
public void closeThisStage(ActionEvent action){
	 Node  source = (Node)  action.getSource(); 
	 Stage stage  = (Stage) source.getScene().getWindow();
    stage.close();
}
@FXML 
private void OpenDBPaneAction(ActionEvent action){
	try {
		Stage DBManagement = new Stage();
		Parent page= FXMLLoader.load(getClass().getResource("DBManagementPane.fxml"));
		Scene scene= new Scene(page);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		DBManagement.setScene(scene);
		DBManagement.show();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}


@Override
public void initialize(URL location, ResourceBundle resources) {
	setup.StartWithItems();
	//tarkistus, onko tietokannassa smartposteja, jos ei, haetaan xml:stä
	checker = setup.getCityNames();
	if(checker.size()== 0){
		try {
			setup.parseOfficeListing();
			checker = setup.getCityNames();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	CitySelector.setItems(FXCollections.observableArrayList(checker));
	//jos tietokannassa ei ole esineitä lisätään sinne esimerkkiesineet
	if(setup.getItems().size()== 0){
		setup.setItems();
	}
	if (ParcelList.size() == 0){
		ParcelList = setup.getParcelsFromDB();
	}
	
	//ladataan kartta
	mapSlinger.getEngine().load(getClass().getResource("index.html").toExternalForm());
	
} 
}
