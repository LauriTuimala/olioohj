package application;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
//Tämä classi on ohjelman loggaaja; kaikki käyttäjän tekemät merkitsevät teot kirjataan listaan joka printataan tiedostoon ajon lopuksi.
public class ActionLog {
private ArrayList<String> actions= new ArrayList<>();

static private ActionLog logger= null;

//Singleton
public static ActionLog getLister(){
    if (logger == null){
   	 logger = new ActionLog();
    }
    return logger;
	}
//Varsinainen lokiin kirjaus, timestampin kera. 
public void Logging(String s){
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy - HH:mm:ss");
	LocalDateTime now = LocalDateTime.now();
	actions.add(dtf.format(now) + " -User " + s);
	System.out.println(actions.get((actions.size()-1)));
}

//Login kirjoitus tiedostoon
public void PrintLog(){
	String fileout = "Logfile.txt";
	
	try {
			PrintWriter out = new PrintWriter(fileout);
		    for (String line : actions) {
		        out.println(line);
		    }
		    out.close();
		}
	catch(IOException e) {
	// TODO Auto-generated catch block
		   e.printStackTrace();
		}
	
}
}