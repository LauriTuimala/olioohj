package application;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
//Luokka jolla voidaan poistaa tietoja DB:stä GUI:n avulla
public class DBManagementController implements Initializable {
	SmartRunner setup = SmartRunner.getLister();
	ActionLog logger = ActionLog.getLister();
	ArrayList<Parcel> ParcelDBList= new ArrayList<>();
	ArrayList<Item> ItemDBList = new ArrayList<>();
	ArrayList<Office> CityNodes= new ArrayList<>();
	ArrayList<String> checker = new ArrayList();
@FXML
private Button AddAllItems;
@FXML
private Button AddAllPackages;
@FXML
private Button ClearItems;
@FXML
private Button ClearPackages;
@FXML
private Button DeleteItem;
@FXML
private Button DeletePackage;
@FXML
private Button DeleteSmartPost;
@FXML
private Button ReloadSmartPosts;
@FXML
private Button CloseDBPane;
@FXML
private ComboBox<Item> ItemDeletionChoice;
@FXML
private ComboBox<Parcel> PackageDeletionChoice;
@FXML
private ComboBox<String> CityChoice;
@FXML
private ChoiceBox<Office> OfficeChoice;

@FXML //lisätään käytön aikaiset esineet tietokantaan, poistetaan vanhat skräbät tietokannan eheyden nimissä
private void AddAllItemsAction(ActionEvent action){
	setup.DeleteTableContents("ITEMS");
	//muokataan ID:t järkeviksi ennen tietokantaan tunkemista
	int j= 1;
	for(Item append : setup.getItems()){
		append.setItemID(j);
		setup.addItemtoDb(append);
		j++;
	}
	setup.clearItemList();
	ItemDBList = setup.getItemsFromDB();
	ItemDeletionChoice.setItems(FXCollections.observableArrayList(ItemDBList));
}
@FXML //lisätään käytön aikaiset paketit tietokantaan, poistetaan vanhat skräbät tietokannan eheyden nimissä
private void AddAllPackagesAction(ActionEvent action){
	setup.DeleteTableContents("PACKAGE");
	//muokataan ID:t järkeviksi ennen tietokantaan tunkemista
	int i = 1;
	for(Parcel append : setup.getParcels()){
		append.setParcelID(i);
		setup.addParceltoDb(append);
		i++;
	}
	setup.clearParcelList();
	ParcelDBList = setup.getParcelsFromDB();
	PackageDeletionChoice.setItems(FXCollections.observableArrayList(ParcelDBList));
}


@FXML //Tyhjentää tietokannasta taulun ITEMS
private void ClearItemsAction(ActionEvent action){
	setup.DeleteTableContents("ITEMS");
	ItemDBList.clear();
	ItemDeletionChoice.setItems(FXCollections.observableArrayList(ItemDBList));
}
@FXML //Tyhjentää tietokannasta taulun PACKAGE
private void ClearPackagesAction(ActionEvent action){
	setup.DeleteTableContents("PACKAGE");
	ParcelDBList.clear();
	PackageDeletionChoice.setItems(FXCollections.observableArrayList(ParcelDBList));
}
@FXML //poistaa halutun esineen tietokannasta
private void DeleteItemAction(ActionEvent action){
	if(ItemDeletionChoice.getValue()!= null){
		setup.DeleteItemFromDb(ItemDeletionChoice.getValue());
		ItemDBList.remove(ItemDeletionChoice.getValue());
		ItemDeletionChoice.setItems(FXCollections.observableArrayList(ItemDBList));
	}
	
}
@FXML //poistaa halutun paketin tietokannasta
private void DeletePackageAction(ActionEvent action){
	if(PackageDeletionChoice.getValue()!= null){
		setup.DeleteParcelFromDb(PackageDeletionChoice.getValue());
		ParcelDBList.remove(PackageDeletionChoice.getValue());
		PackageDeletionChoice.setItems(FXCollections.observableArrayList(ParcelDBList));
	}
}
@FXML //poistaa halutun SmartPostin tietokannasta
private void DeleteSmartPostAction(ActionEvent action){
	if(OfficeChoice.getValue()!= null){
		setup.DeleteOfficeFromDb(OfficeChoice.getValue());
		checker = setup.getCityNames();
		CityChoice.setItems(FXCollections.observableArrayList(checker));
	}
}
@FXML   //poistaa Smartpostit tietokannasta, lataa ne uudestaan .XML:stä
private void ReloadSmartPostAction(ActionEvent action){
	try {
		setup.DeleteTableContents("OFFICES");
		setup.parseOfficeListing();
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();	
	}
	
}
@FXML
private void CloseDBPaneAction(ActionEvent action){
	 Node  source = (Node)  action.getSource(); 
	 Stage stage  = (Stage) source.getScene().getWindow();
     stage.close();
}



	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ParcelDBList = setup.getParcelsFromDB();
		ItemDBList = setup.getItemsFromDB();
		checker = setup.getCityNames();
		CityChoice.setItems(FXCollections.observableArrayList(checker));
		ItemDeletionChoice.setItems(FXCollections.observableArrayList(ItemDBList));
		PackageDeletionChoice.setItems(FXCollections.observableArrayList(ParcelDBList));
		CityChoice.valueProperty().addListener(new ChangeListener<String>(){
		
			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				CityNodes= setup.getSelectCityFromDb(arg2);
				OfficeChoice.setItems(FXCollections.observableArrayList(CityNodes));
			}
			
		});
	}
}
