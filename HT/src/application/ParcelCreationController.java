package application;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class ParcelCreationController implements Initializable{
	SmartRunner setup = SmartRunner.getLister();
	ActionLog logger = ActionLog.getLister();
	
	@FXML
	private Button CloseParcelCreationButton;
	@FXML
	private ChoiceBox<String> Itempicker;
	@FXML
	private ChoiceBox<String> SizeSelector;
	@FXML
	private ChoiceBox<String> Fragility;
	@FXML
	private ChoiceBox<String> QualitySelector;
	@FXML
	private TextField NameField;
	@FXML
	private TextArea ErrorBox;
	
	
	
	@FXML //sulkee ikkunan ja tekee uuden paketin käyttäjän syötteiden mukaan
	private void CloseParcelPane(ActionEvent action){
	
		if ((Itempicker.getValue())!= null){
			//etsii esinevalikosta valitun esineen ja paketoi sen
			PackageItem(setup.findItem(Itempicker.getValue()));
			closeThisStage(action);
		}else if(SizeSelector.getValue() == null){
			//varmistetaan käyttäjän antaneen tarvittavat tiedot
			ErrorBox.setText("Anna esineelle koko");
		}else if(Fragility.getValue() == null){
			//lisää syötöntarkistusta
			ErrorBox.setText("Anna esineelle särkyvyysluokitus");
		}else if((NameField.getText()=="")|| (NameField.getText() == NameField.getPromptText())){
			//virheentarkistusta
			ErrorBox.setText("Anna esineelle nimi");
			
		}else{
			//uuden esineen luonti käyttäjän antamien tietojen mukaan
			Item uudehko = new Item(NameField.getText(), SizeSelector.getValue(), Fragility.getValue());
			setup.getItems().add(uudehko);
			logger.Logging("created new item "+NameField.getText());
			PackageItem(uudehko);
			Itempicker.setItems(FXCollections.observableArrayList(setup.getItemNames()));
			closeThisStage(action);
		}
		
	}
	//metodi joka paketoi esineen
	public void PackageItem(Item uusi){
		if(QualitySelector.getValue() != null){
			switch(Integer.parseInt(QualitySelector.getValue())){
			case 1:
				FirstClassParcel uuspaketti = new FirstClassParcel(uusi); 
				setup.getParcels().add(uuspaketti);
				logger.Logging("created new parcel "+uuspaketti.getParcelID()+ " that contains "+ uuspaketti.contents.getName());
				break;
			case 2:
				if (uusi.getSize()== "Suuri"){
					ErrorBox.setText("Liian suuri esine tähän pakettiluokkaan");
				}else{
					SecondClassParcel uuskakkospaketti = new SecondClassParcel(uusi); 
					setup.getParcels().add(uuskakkospaketti);
					logger.Logging("created new parcel "+uuskakkospaketti.getParcelID()+ " that contains "+ uuskakkospaketti.contents.getName());
				}
					break;
			case 3:	
				ThirdClassParcel uuskolmospaketti = new ThirdClassParcel(uusi);
				setup.getParcels().add(uuskolmospaketti);
				logger.Logging("created new parcel "+uuskolmospaketti.getParcelID()+ " that contains "+ uuskolmospaketti.contents.getName());
				break;
				}
		}else{
		ErrorBox.setText("Anna paketille luokka");
		}
	}
	
	public void closeThisStage(ActionEvent action){
		 Node  source = (Node)  action.getSource(); 
		 Stage stage  = (Stage) source.getScene().getWindow();
	     stage.close();
	}
	
	@Override
	public void initialize(URL location,ResourceBundle resources) {
		QualitySelector.setItems(FXCollections.observableArrayList("1","2", "3"));
		SizeSelector.setItems(FXCollections.observableArrayList("Pieni","Medium", "Suuri"));
		Fragility.setItems(FXCollections.observableArrayList("Kyllä","Ei"));
		Itempicker.setItems(FXCollections.observableArrayList(setup.getItemNames()));
	}


		



}
