package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class browsaController {
	@FXML
	private Button Start;
	
	@FXML
	private void startBrowser(ActionEvent event){
		try {
			Stage webBrowser = new Stage();
			Parent page= FXMLLoader.load(getClass().getResource("WebBrowser.fxml"));
			Scene scene= new Scene(page);
			webBrowser.setScene(scene);
			webBrowser.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
