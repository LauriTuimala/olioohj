package application;


import java.net.URL;
import java.util.Deque;
import java.util.LinkedList;

import java.util.ResourceBundle;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;

public class WebBrowserController implements Initializable{ 
@FXML
private GridPane pane1;
@FXML
private GridPane pane2;
@FXML
private Button Refresh;
@FXML
private Button Back;
@FXML
private Button Fwd;
@FXML
private TextField addressBar;
@FXML
private WebView webSlinger;
private String currentAddress="http:\\www.lut.fi";
private Deque<String> AddressmemBack = new LinkedList<String>();
private Deque<String> AddressmemFwd = new LinkedList<String>();

@FXML
private void addressBarAction(KeyEvent event){
	if(event.getCode().equals(KeyCode.ENTER)){
		AddressmemBack.addFirst(currentAddress);
	    if(AddressmemBack.size() >= 10){
			AddressmemBack.removeLast();
		} 
		String goAddress = "http:\\"+addressBar.getText();
		webSlinger.getEngine().load(goAddress);
		currentAddress = goAddress;
		AddressmemFwd.clear();
    }
}
@FXML
private void BackButtonAction(ActionEvent event){
	if(!AddressmemBack.isEmpty()){
		String go = AddressmemBack.pollFirst();
		AddressmemFwd.addFirst(currentAddress);
		webSlinger.getEngine().load(go);
		currentAddress = go;
	}
}
@FXML
private void ForwardButtonAction(ActionEvent event){
	if(!AddressmemFwd.isEmpty()){
		String go = AddressmemFwd.pollFirst();
		AddressmemBack.addFirst(currentAddress);
		webSlinger.getEngine().load(go); 
		currentAddress = go;
	}
	
}
@FXML private void RefreshButtonAction(ActionEvent event){
	webSlinger.getEngine().load(currentAddress);
}

@FXML
private void loadLocalFile(ActionEvent event){
	webSlinger.getEngine().load(getClass().getResource("index2.html").toExternalForm());
}
@FXML
private void doJavaScriptshit(ActionEvent event){
	webSlinger.getEngine().executeScript("document.shoutOut()");
}
@FXML
private void doJavaScriptstuff(ActionEvent event){
	webSlinger.getEngine().executeScript("initialize()");
}
@Override
public void initialize(URL location, ResourceBundle resources) {
	webSlinger.getEngine().load("http:\\www.lut.fi");
}

}
