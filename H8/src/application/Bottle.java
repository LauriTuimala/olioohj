package application;



public class Bottle {
	private String name;
	private String manuf;
	private float energy;
	private float price;
	private float volume;
	Bottle(){
		name= "Pepsi Max";
		manuf = "Pepsi";
		energy = 0.3f;
		price = 1.8f;
		volume= 0.5f;
		}
	public String getName(){
		return name;
	}
	public float getPrice(){
		return price;
	}
	public float getVolume(){
		return volume;
	}
	@Override
	public String toString(){
		return name;
	}

	Bottle(String n, String m, float k, float h, float v){
		name= n;
		manuf =m;
		energy =k;
		price = h;
		volume= v;
	}
}


