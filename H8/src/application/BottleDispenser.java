package application;

	import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

	public class BottleDispenser {
		private int bottles;
	    private static float money;
	    private ArrayList<Bottle> Bottles = new ArrayList();
	    private String name;
	    static private BottleDispenser maatti= null;
	    private Bottle reseptiPullo = null;
	    private BottleDispenser() {

	    	bottles = 6;
	        money = 0;
	        addBottle(new Bottle());
	        addBottle(new Bottle("Pepsi Max","Pepsi", 0.4f, 2.2f, 1.5f));
	        addBottle(new Bottle("Coca-Cola Zero","Coca-Cola", 0.5f, 2.0f, 0.5f));
	        addBottle(new Bottle("Coca-Cola Zero","Coca-Cola", 0.5f, 2.5f, 1.5f));
	        addBottle(new Bottle("Fanta Zero","Coca-Cola", 0.4f, 1.95f, 0.5f));
	        addBottle(new Bottle("Fanta Zero","Coca-Cola", 0.4f, 2.10f, 1.0f));
	    }
	    public static BottleDispenser getDispenser(){
		       if (maatti == null){
		    	 maatti = new BottleDispenser();
		       }
		       return maatti;
	    }
	    public void addBottle(Bottle d){
	    	Bottles.add(d);
	    }
	    public int findABottle(String f){
	    	int i;
	    	for (i= 0;i< Bottles.size(); i++){
	    		if (f.equals(Bottles.get(i).getName())){
	    				return i;		
	    				
	    			}
	    			
	    	}
	    	return 50;
	    }
	    
	    public int findBottle(String f, float k){
	    	int i;
	    	for (i= 0;i< Bottles.size(); i++){
	    		if (f.equals(Bottles.get(i).getName())){
	    			if (k == Bottles.get(i).getVolume()){
	    				return i;
	    		
	    				}
	    			}
	    			
	    	}
	    	return 50;
	    }
	    public float getMoney(){
	    	return money;
	    }
	    
	    public ArrayList<Bottle> getBottles(){
	    	return Bottles;
	    } 
	    public Bottle getBottle(int i){
	    	if (Bottles.size() > i){
	    		return Bottles.get(i);
	    	}else
	    		return new Bottle("Error","rorrE", 6.00f, 4.00f, 5.00f);
	    }
	    public static void addMoney(float n) {
	        money += n;
	     }

	    public void listBottles(){
	    	int k;
	    	for(k=0;k<Bottles.size();k++){
	    		Bottle placeholder= getBottle(k);
	    		System.out.println(k+1+". Nimi: " +placeholder.getName());
	    		System.out.println("\t"+"Koko: " +placeholder.getVolume()+"\t"+"Hinta: "+placeholder.getPrice());
	    	}
	    }
	    public String buyBottle(int i) {
	    	if (i == 50){
	    		return ("Valitsemaanne kokoa ei löydy");
	    	}
	    	Bottle ostopullo = getBottle(i);
	       	if((Bottles.size() > 0)&&(money >= ostopullo.getPrice())){ 
	       		money -= ostopullo.getPrice();
	       		reseptiPullo= ostopullo;
	       		Bottles.remove(i);
	       		return("KACHUNK! " +ostopullo.getName()+ " tipahti masiinasta!");
	       	}else if (money<ostopullo.getPrice()){
	    	  return("Syötä rahaa ensin!");
	       }else {
	    	   return("Automaatti on tyhjä");
	       }
	   }
	    public void writeReceipt(){
	    	if (reseptiPullo!= null){
	    		String line = ("Kuitti: " + reseptiPullo.getName() + " Hinta :" + String.valueOf(reseptiPullo.getPrice()));
	    		String fileout = "Kuitti.txt";
	    		BufferedWriter out;
	    		try {
	    			out = new BufferedWriter(new FileWriter(fileout));
	    			out.write(line);
	    			out.close();
	    		} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	    		}
	    		reseptiPullo = null;
	    	}
	    	
	    }
	    public void returnMoney() {
	       
	    	System.out.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n", money);
	        money = 0;
	    }
	  
	}

