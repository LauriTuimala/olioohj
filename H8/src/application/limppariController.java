package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Slider;
import application.BottleDispenser;
import application.Bottle;

public class limppariController implements Initializable{
	@FXML
	private Button increaseMoney; 
	@FXML
	private Slider moneySlider;
	@FXML
	private Button Buy;
	@FXML
	private TextArea bbs;
	@FXML
	private Label cost;
	@FXML
	private Label moneyLabel;
	@FXML
	private TextField Price;
	@FXML
	private TextField moneyToBeAdded;
	@FXML
	private TextField availableMoney;
	@FXML
	private ComboBox<Bottle> beverages;
	@FXML
	private ComboBox<String> sizePicker;
	@FXML 
	private Button WriteReceipt;


	private BottleDispenser maatti; 
	
	@FXML
	private void increaseMoneyAction(ActionEvent event){
		//BottleDispenser maatti = BottleDispenser.getDispenser();
		BottleDispenser.addMoney(Float.parseFloat(moneyToBeAdded.getText()));
		availableMoney.setText((String.valueOf(maatti.getMoney())));
		bbs.setText(bbs.getText() + "\n"+"Automaattiin lisättiin "+moneyToBeAdded.getText()+ " rahaa");
		moneyToBeAdded.setText("0");
		moneySlider.setValue(0);
		
	} 
	@FXML 
	private void addincrease(MouseEvent event){
		moneyToBeAdded.setText(String.valueOf((moneySlider.getValue())));
		
	}
	@FXML
	private void buyBeverageAction(ActionEvent event){
		//BottleDispenser maatti = BottleDispenser.getDispenser();
		if (maatti.getBottles().isEmpty()){
		bbs.setText(bbs.getText()+"\n" +"Automaatti on tyhjä :(");
		}else{
		int j =maatti.findBottle((beverages.getValue().getName()), Float.parseFloat(sizePicker.getValue()));
		String msg1= maatti.buyBottle(j);
    	beverages.setItems(FXCollections.observableArrayList(maatti.getBottles()));
		availableMoney.setText((String.valueOf(maatti.getMoney())));
		bbs.setText(bbs.getText()+"\n" +msg1);

	}}
		
	@FXML
	private void setBeverage(MouseEvent event){
		//BottleDispenser maatti = BottleDispenser.getDispenser();
		try{
			int j =maatti.findABottle((beverages.getValue().getName()));
			if (j!= 50){
				Price.setText(String.valueOf((maatti.getBottle(j).getPrice())));
				System.out.println(String.valueOf((maatti.getBottle(j).getPrice())));
		}
		}catch(NullPointerException e){
			//e.printStackTrace();		
		}
		
	
	}
	@FXML
	private void writeReceiptAction(ActionEvent event){
		BottleDispenser maatti = BottleDispenser.getDispenser();
		maatti.writeReceipt();
		
	}
	
	
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	maatti = BottleDispenser.getDispenser();
    	sizePicker.setItems(FXCollections.observableArrayList("0.33", "0.5", "1.0", "1.5"));
    	beverages.setItems(FXCollections.observableArrayList(maatti.getBottles()));
		
    }

    
}
