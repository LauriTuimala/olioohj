package application;
	
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("limppari.fxml"));
			Scene scene = new Scene(root,400,400);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		launch(args);
	/*	int m= 1;
		int valinta;
		int j=1;
		while(m >0){
			System.out.println();
			System.out.println("*** LIMSA-AUTOMAATTI ***");
			System.out.println("1) Lisää rahaa koneeseen");
			System.out.println("2) Osta pullo");
			System.out.println("3) Ota rahat ulos");
			System.out.println("4) Listaa koneessa olevat pullot");
			System.out.println("0) Lopeta");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Valintasi: ");
			valinta= Integer.parseInt(br.readLine());
			switch(valinta) {
				case 1:	automaatti.addMoney();
						break;
				case 2: automaatti.listBottles();
						BufferedReader bra = new BufferedReader(new InputStreamReader(System.in));
						System.out.print("Valintasi: ");
						j= Integer.parseInt(bra.readLine());
						automaatti.buyBottle(j);
						break;	
				case 3: automaatti.returnMoney();
						break;
				case 4: automaatti.listBottles();
						break;
				case 0: m = 0;		
						break;
				default: System.out.println("Väärä syöte");
						break;
				
			}
		
		}*/	
	
	}
	}

