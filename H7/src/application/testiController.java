package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
 
public class testiController implements Initializable {
	
	
    @FXML
    private Label indicator;
    @FXML
    private Label indicator2;
    @FXML
    private Label showMe;
    @FXML
    private Label label;
    @FXML
    private TextField inputField;    
    @FXML
    private TextArea  wallOfText;
    @FXML
    private Button Load;
    @FXML
    private Button Save;
    
    
    @FXML
    private void loadButtonAction(ActionEvent event) throws IOException {
    	String filein = showMe.getText();
    	filein = filein.trim();
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(filein));
			String rivi;
			while((rivi = br.readLine()) != null) {
	            wallOfText.setText( wallOfText.getText()+"\n"+rivi);
	        }
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
        label.setText("File Loaded");
        
    }
    @FXML
    private void saveButtonAction(ActionEvent event)throws IOException {
    	String fileout = showMe.getText();
    	fileout = fileout.trim();
    	BufferedWriter out;
		try {
			out = new BufferedWriter(new FileWriter(fileout));
			out.write(wallOfText.getText());
		    out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		label.setText("Text Saved");
   
    }
    
    @FXML
    private void copyInputField(KeyEvent event){
    	if(event.getCode().equals(KeyCode.ENTER)){
    			System.out.println("MOI!");
    			showMe.setText(inputField.getText());
    	}
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

		
    }    
    
    
 
    
    
    
}




